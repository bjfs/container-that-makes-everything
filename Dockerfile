FROM debian:buster

# Start from building stuff for the Python OpenStack client
RUN apt update && apt upgrade -y && apt install -y curl libssl-dev python3-pip rustc
RUN pip3 install python-openstackclient

# Google Cloud SDK needs to be installed as local user 
RUN useradd -ms /bin/bash debian
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-336.0.0-linux-x86_64.tar.gz \
    && tar zxvf google-cloud-sdk-336.0.0-linux-x86_64.tar.gz && chown -R debian:debian google-cloud-sdk \
    && mv google-cloud-sdk /home/debian
USER debian
WORKDIR /home/debian
RUN cd google-cloud-sdk && ./install.sh -q
RUN bash -c "source google-cloud-sdk/path.bash.inc && gcloud components install alpha beta kubectl"
