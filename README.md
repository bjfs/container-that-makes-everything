**A Container that Makes Everything**

Special purpose image used on a device like NAS to do random stuff where running fully-fledged VM is otherwise impossible.

---

## Can I has... ?! 

1. Has all the Python I need.
2. Has OpenStack client, too.
3. Has Google Cloud SDK.
4. Has AWS CLI v2 (well, not yet)

Therefore it can synchronize with all things cloud (apart Azure which we don't like) and local mounts, of course.

**Stay tuned!**
